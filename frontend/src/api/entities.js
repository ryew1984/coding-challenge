import axios from 'axios';

export default {
  getEntities() {
    return axios.get('http://localhost:3000/entities');
  },
};
