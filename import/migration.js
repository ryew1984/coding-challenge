const neo4j = require('neo4j-driver');
const { map } = require('rxjs/operators');

const driver = neo4j.driver('neo4j://localhost', neo4j.auth.basic('neo4j', 'neo'), {
  maxTransactionRetryTime: 30000,
});

const rxSession = driver.rxSession();

rxSession
  .writeTransaction((txc) => txc
    .run('CALL apoc.load.json("https://gist.githubusercontent.com/leefreemanxyz/903b8d090e8cb7caa82ab2ebf17c6a35/raw/bee05f3ac180d8b0e202264f74926c96145b8be7/nerf-herders-test-data") YIELD value\n'
                + 'UNWIND value.data AS item\n'
                + '// RETURN item.name, item.description, item.parent;\n'
                + 'MERGE (c:Entity {name:item.name})\n'
                + 'SET c.parent = item.parent\n'
                + 'SET c.description = item.description\n'
                + 'WITH * WHERE item.parent <> ""\n'
                + 'MERGE (p:Entity {name: item.parent})\n'
                + 'MERGE (p)<-[:children]-(c);')
    .records()
    .pipe(map((record) => record.get('name'))))
  .subscribe({
    // eslint-disable-next-line no-console
    next: (data) => console.log(data),
    complete: () => {
      // eslint-disable-next-line no-console
      console.log('migration completed');
      rxSession.close();
      driver.close();
    },
    // eslint-disable-next-line no-console
    error: (error) => console.log(error),
  });
